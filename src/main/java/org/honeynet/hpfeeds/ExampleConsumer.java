package org.honeynet.hpfeeds;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.honeynet.hpfeeds.Hpfeeds.EOSException;
import org.honeynet.hpfeeds.Hpfeeds.InvalidStateException;
import org.honeynet.hpfeeds.Hpfeeds.LargeMessageException;
import org.honeynet.hpfeeds.Hpfeeds.ReadTimeOutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExampleConsumer {
	
	private static Logger log = LoggerFactory.getLogger(ExampleConsumer.class);
	
	
	public static void printUsage() {
		System.err.println("usage:  -h <host> -p <port> -i <ident> -s <secret> -c <channel>[,channel2,channel3 ...]");
	}
	
	public static void main(String args[]) throws IOException, ReadTimeOutException, EOSException, LargeMessageException, InvalidStateException {
		Map<String,String> argMap = new HashMap<String,String>();
		argMap.put("-h", null);
		argMap.put("-p", null);
		argMap.put("-i", null);
		argMap.put("-s", null);
		argMap.put("-c", null);
		
		String arg = null;
		for (String a : args) {
			if (arg == null) {
				if (argMap.containsKey(a)) {
					arg = a;
					continue;
				}
				else {
					System.err.println("invalid argument: " + a);
					printUsage();
					System.exit(1);
				}
			} else {
				argMap.put(arg, a);
				arg = null;
			}
		}
		
		if (argMap.containsValue(null)) {
			System.err.println("missing argument(s)");
			printUsage();
			System.exit(1);
		}
		
		
		String host = argMap.get("-h");
		int port = Integer.parseInt(argMap.get("-p"));
		String ident = argMap.get("-i");
		String secret = argMap.get("-s");
		String[] channels = argMap.get("-c").split(",");
		
		
		Hpfeeds h = new Hpfeeds(host, port, ident, secret);
		h.connect();
		h.subscribe(channels);
		h.run(new ExampleHandler(), new ExampleErrorHandler());
		
	}
	
	
	public static class ExampleHandler implements Hpfeeds.MessageHandler {
		private Pattern pat = Pattern.compile(".*\\P{Print}");
		
		@Override
		public void onMessage(String ident, String chan, ByteBuffer msg) {
			try {
				String out = null;
				boolean notPrintable = false;
				
				ByteBuffer buf = msg.slice();
				int bufLen = buf.remaining();
				if (bufLen > 20) {
					buf.limit(20);
					bufLen = 20;
				}
				
				try {
					notPrintable = pat.matcher(Hpfeeds.decodeString(buf)).matches();
				} catch (CharacterCodingException e) {
					notPrintable = true;
				}
				
				if (notPrintable) {
					StringBuilder outBuilder = new StringBuilder(64);
					buf.rewind();
					for (int i = bufLen ; i != 0 ; i--) {
						outBuilder.append(String.format("%02x ", buf.get()));
					}
					outBuilder.append("...");
					out = outBuilder.toString();
				}
				else {
					out = Hpfeeds.decodeString(msg);
				}
				
				log.info("publish to {} by {}: {}", new Object[]{chan, ident, out});
			}
			catch (CharacterCodingException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	
	public static class ExampleErrorHandler implements Hpfeeds.ErrorHandler {
		@Override
		public void onError(ByteBuffer msg) {
			try {
				log.error("error message from broker: {}", Hpfeeds.decodeString(msg));
				System.exit(1);
			}
			catch (CharacterCodingException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
