package org.honeynet.hpfeeds;

import java.io.IOException;
import java.io.InputStream;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.honeynet.hpfeeds.Hpfeeds.EOSException;
import org.honeynet.hpfeeds.Hpfeeds.InvalidStateException;
import org.honeynet.hpfeeds.Hpfeeds.LargeMessageException;
import org.honeynet.hpfeeds.Hpfeeds.ReadTimeOutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;


public class ExampleCsvConsumer {
	
	private static Logger log = LoggerFactory.getLogger(ExampleCsvConsumer.class);
	
	
	public static void printUsage() {
		System.err.println("usage:  -h <host> -p <port> -i <ident> -s <secret> -c <channel>");
	}
	
	public static void main(String args[]) throws IOException, EOSException, ReadTimeOutException, LargeMessageException, InvalidStateException {
		Map<String,String> argMap = new HashMap<String,String>();
		argMap.put("-h", null);
		argMap.put("-p", null);
		argMap.put("-i", null);
		argMap.put("-s", null);
		argMap.put("-c", null);
		
		String arg = null;
		for (String a : args) {
			if (arg == null) {
				if (argMap.containsKey(a)) {
					arg = a;
					continue;
				}
				else {
					System.err.println("invalid argument: " + a);
					printUsage();
					System.exit(1);
				}
			} else {
				argMap.put(arg, a);
				arg = null;
			}
		}
		
		if (argMap.containsValue(null)) {
			System.err.println("missing argument(s)");
			printUsage();
			System.exit(1);
		}
		
		
		String host = argMap.get("-h");
		int port = Integer.parseInt(argMap.get("-p"));
		String ident = argMap.get("-i");
		String secret = argMap.get("-s");
		String channel = argMap.get("-c");
		
		
		Hpfeeds h = new Hpfeeds(host, port, ident, secret);
		h.connect();
		h.subscribe(new String[]{channel});
		h.run(new CsvHandler(), new ExampleErrorHandler());
		
	}
	
	
	public static class CsvHandler implements Hpfeeds.MessageHandler {
		private ObjectMapper jsonObjectMapper = new ObjectMapper();
		private CsvMapper csvObjectMapper = new CsvMapper();
		CsvSchema csvShema = csvObjectMapper.schemaFor(Attack.class);
		
		private static class Attack {
			public long timestamp;
			public String ident, chan;
			public String daddr, dport, saddr, sport, md5, sha512, url;
			
			@Override
			public String toString() {
				return String.format("%d %s %s %s %s %s %s %s %s %s", timestamp, ident, chan, daddr, dport, saddr, sport, md5, sha512, url);
			}
		}
		
		@Override
		public void onMessage(String ident, String chan, ByteBuffer msg) {
			long timestamp = new Date().getTime();
			try {
				Attack attack = jsonObjectMapper.readValue(new ByteBufferInputStream(msg), Attack.class);
				attack.timestamp = timestamp;
				attack.ident = ident;
				attack.chan = chan;
				//FIXME double and triple commas?
				String csv = csvObjectMapper.writer().withSchema(csvShema).writeValueAsString(attack);
				System.out.print(csv);
			}
			catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	public static class ByteBufferInputStream extends InputStream {
		private ByteBuffer buf;
		
		public ByteBufferInputStream(ByteBuffer buf) {
			this.buf = buf;
		}

		@Override
		public int read() throws IOException {
			try {
				return buf.get() & 0xff;
			}
			catch (BufferUnderflowException e) {
				return -1;
			}
		}
	}
	
	
	public static class ExampleErrorHandler implements Hpfeeds.ErrorHandler {
		@Override
		public void onError(ByteBuffer msg) {
			try {
				log.error("error message from broker: {}", Hpfeeds.decodeString(msg));
				System.exit(1);
			}
			catch (CharacterCodingException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
