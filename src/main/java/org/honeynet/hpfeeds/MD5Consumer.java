package org.honeynet.hpfeeds;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import org.honeynet.hpfeeds.Hpfeeds.EOSException;
import org.honeynet.hpfeeds.Hpfeeds.InvalidStateException;
import org.honeynet.hpfeeds.Hpfeeds.LargeMessageException;
import org.honeynet.hpfeeds.Hpfeeds.ReadTimeOutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MD5Consumer {
	
	private static Logger log = LoggerFactory.getLogger(MD5Consumer.class);
	
	
	public static void printUsage() {
		System.err.println("usage:  -h <host> -p <port> -i <ident> -s <secret> -c <channel>[,channel2,channel3 ...]");
	}
	
	public static void main(String args[]) throws IOException, ReadTimeOutException, EOSException, LargeMessageException, InvalidStateException {
		Map<String,String> argMap = new HashMap<String,String>();
		argMap.put("-h", null);
		argMap.put("-p", null);
		argMap.put("-i", null);
		argMap.put("-s", null);
		argMap.put("-c", null);
		
		String arg = null;
		for (String a : args) {
			if (arg == null) {
				if (argMap.containsKey(a)) {
					arg = a;
					continue;
				}
				else {
					System.err.println("invalid argument: " + a);
					printUsage();
					System.exit(1);
				}
			} else {
				argMap.put(arg, a);
				arg = null;
			}
		}
		
		if (argMap.containsValue(null)) {
			System.err.println("missing argument(s)");
			printUsage();
			System.exit(1);
		}
		
		
		String host = argMap.get("-h");
		int port = Integer.parseInt(argMap.get("-p"));
		String ident = argMap.get("-i");
		String secret = argMap.get("-s");
		String[] channels = argMap.get("-c").split(",");
		
		
		Hpfeeds h = new Hpfeeds(host, port, ident, secret);
		h.connect();
		h.subscribe(channels);
		h.run(new MD5Handler(), new ExampleErrorHandler());
		
	}
	
	
	public static class MD5Handler implements Hpfeeds.MessageHandler {
		private MessageDigest md;
		
		public MD5Handler() {
			try {
				md = MessageDigest.getInstance("MD5");
			}
			catch (NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
		}
		
		@Override
		public void onMessage(String ident, String chan, ByteBuffer msg) {
			int len = msg.remaining();
			md.reset();
			md.update(msg);	
			
			String md5 = new BigInteger(1, md.digest()).toString(16);
			// pad
			if (md5.length() < 32) {
				StringBuilder sb = new StringBuilder(32);
				int i = 32 - md5.length();
				while (i > 0) {
					sb.append("0");
					i--;
				}
				sb.append(md5);
				md5 = sb.toString();
			}
			log.info("publish to {} by {}: length={} md5={}", new Object[]{chan, ident, len, md5});
		}
	}
	
	
	public static class ExampleErrorHandler implements Hpfeeds.ErrorHandler {
		@Override
		public void onError(ByteBuffer msg) {
			try {
				log.error("error message from broker: {}", Hpfeeds.decodeString(msg));
				System.exit(1);
			}
			catch (CharacterCodingException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
