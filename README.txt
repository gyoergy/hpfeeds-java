== Compiling ==
 1. mvn compile
 2. mvn dependency:copy-dependencies 

== Using the Examples ==
 1. java -cp target/dependency/*:target/classes org.honeynet.hpfeeds.ExampleConsumer -h <host> -p <port> -i <ident> -s <secret> -c <channel>[,channel2,channel3 ...]
 2. java -cp target/dependency/*:target/classes org.honeynet.hpfeeds.ExampleCsvConsumer -h <host> -p <port> -i <ident> -s <secret> -c <channel>
 3. java -cp target/dependency/*:target/classes org.honeynet.hpfeeds.MD5Consumer -h <host> -p <port> -i <ident> -s <secret> -c <channel>[,channel2,channel3 ...]
 4. java -cp target/dependency/*:target/classes org.honeynet.hpfeeds.FilePublisher -h <host> -p <port> -i <ident> -s <secret> -c <channel> -f <file>

